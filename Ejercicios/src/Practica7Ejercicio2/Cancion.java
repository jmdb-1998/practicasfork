package Practica7Ejercicio2;

public class Cancion {

	private String titulo;
	private String artista;
	private String duracion;
	private String genero;
	
	
	public Cancion() {
		
		this.setArtista(null);
		this.setGenero(null);
		this.setTitulo(null);
		this.setDuracion("0");
	}
	
	public Cancion(String titulo, String artista, String duracion, String genero) {
		
		this.setArtista(artista);
		this.setGenero(genero);
		this.setTitulo(titulo);
		this.setDuracion(duracion);
		
	}
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getArtista() {
		return artista;
	}
	public void setArtista(String artista) {
		this.artista = artista;
	}
	public String getDuracion() {
		return duracion;
	}
	public void setDuracion(String duracion) {
		this.duracion = duracion;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}	
}
