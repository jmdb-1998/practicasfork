package Practica7Ejercicio2;

public class TelefonoEmpresa extends TelefonoTienda {

	private String empleadoActual;
	private int numeroTelefono;
	private boolean asignado;
	
	public TelefonoEmpresa(){

		this.setMarca("Samsung");
		this.setModelo("Galaxy S7");
		this.setSistemaOperativo("Android");
		this.setAsignado(false);
		this.setEmpleadoActual(null);
		this.setNumeroTelefono(000000000);
	}

	public TelefonoEmpresa(String marca, String modelo, String sistemaOperativo, boolean asignado, String empleadoActual, int numeroTelefono){

		this.setMarca(marca);
		this.setModelo(modelo);
		this.setSistemaOperativo(sistemaOperativo);
		this.setAsignado(asignado);
		this.setEmpleadoActual(empleadoActual);
		this.setNumeroTelefono(numeroTelefono);
	}
	
	public String getEmpleadoActual() {
		return empleadoActual;
	}
	public void setEmpleadoActual(String empleadoActual) {
		this.empleadoActual = empleadoActual;
	}
	public int getNumeroTelefono() {
		return numeroTelefono;
	}
	public void setNumeroTelefono(int numeroTelefono) {
		this.numeroTelefono = numeroTelefono;
	}
	public boolean isAsignado() {
		return asignado;
	}
	public void setAsignado(boolean asignado) {
		this.asignado = asignado;
	}
}
