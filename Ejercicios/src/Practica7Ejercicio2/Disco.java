package Practica7Ejercicio2;

public class Disco {

	public String titulo;
	public String artista;
	public String duracion;
	public String genero;
	public int numCanciones;
	
	public Disco() {
		this.setTitulo(titulo);
		this.setArtista(artista);
		this.setDuracion(duracion);
		this.setGenero(genero);
		this.setNumCanciones(numCanciones);
	}
	
	public Disco(String titulo, String artista, String duracion, String genero, int numCanciones) {
		this.setTitulo(titulo);
		this.setArtista(artista);
		this.setDuracion(duracion);
		this.setGenero(genero);
		this.setNumCanciones(numCanciones);
	}
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getArtista() {
		return artista;
	}
	public void setArtista(String artista) {
		this.artista = artista;
	}
	public String getDuracion() {
		return duracion;
	}
	public void setDuracion(String duracion) {
		this.duracion = duracion;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public int getNumCanciones() {
		return numCanciones;
	}
	public void setNumCanciones(int numCanciones) {
		this.numCanciones = numCanciones;
	}
}
