package Practica7Ejercicio2;

public class Poligono {

	private int numeroLados;
	private int longitudLados;
	
	public Poligono() {
		
		this.setNumeroLados(3);
		this.setLongitudLados(1);
	}
	
	public Poligono(int lados, int longitud) {
		
		this.setNumeroLados(lados);
		this.setLongitudLados(longitud);
	}
	
	
	public int getLongitudLados() {
		return longitudLados;
	}

	public void setLongitudLados(int longitudLados) {
		this.longitudLados = longitudLados;
	}
	
	public int getNumeroLados() {
		return numeroLados;
	}
	public void setNumeroLados(int numeroLados) {
		this.numeroLados = numeroLados;
	}
	
}
