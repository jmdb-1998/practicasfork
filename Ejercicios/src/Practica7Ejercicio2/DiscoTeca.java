package Practica7Ejercicio2;

public class DiscoTeca extends Disco {

	public DiscoTeca() {
		this.setTitulo(titulo);
		this.setArtista(artista);
		this.setDuracion(duracion);
		this.setGenero(genero);
		this.setNumCanciones(numCanciones);
	}
	
	public DiscoTeca(String titulo, String artista, String duracion, String genero, int numCanciones) {
		this.setTitulo(titulo);
		this.setArtista(artista);
		this.setDuracion(duracion);
		this.setGenero(genero);
		this.setNumCanciones(numCanciones);
	}
}
