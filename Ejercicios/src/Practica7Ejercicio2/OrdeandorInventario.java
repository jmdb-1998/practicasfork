package Practica7Ejercicio2;

public class OrdeandorInventario extends OrdenadorTienda{

	private String duenoActual;
	private boolean disponible;

	public OrdeandorInventario() {

		this.setMarca("Lenovo");
		this.setModelo("Thinkpad 307");
		this.setDuenoActual("Paquito");
		this.setDisponible(false);
		
	}

	public OrdeandorInventario(String marca, String modelo, String duenoActual, boolean disponible) {

		this.setMarca(marca);
		this.setModelo(modelo);
		this.setDuenoActual(duenoActual);
		this.setDisponible(disponible);
	}
	
	public String getDuenoActual() {
		return duenoActual;
	}

	public void setDuenoActual(String duenoActual) {
		this.duenoActual = duenoActual;
	}

	public boolean isDisponible() {
		return disponible;
	}

	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}

}
