package Practica7Ejercicio2;

public class Carta {

	private String remitente;
	private String destinatario;
	private String tipo;
	
	
	public Carta() {
		
		this.setRemitente("NULL");
		this.setDestinatario("NULL");
		this.setTipo("NULL");
	}
	
	public Carta(String remi, String desti, String tipo) {
		
		this.setRemitente(remi);
		this.setDestinatario(desti);
		this.setTipo(tipo);	
	}
	
	public Carta(Carta carta) {
		
		this.remitente = new String(carta.remitente);
		this.destinatario = new String(carta.destinatario);
		this.tipo = new String(carta.tipo);	
	}
	
	public String getRemitente() {
		return remitente;
	}
	public void setRemitente(String remitente) {
		this.remitente = remitente;
	}
	public String getDestinatario() {
		return destinatario;
	}
	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
}
