package Practica7Ejercicio2;

public class LibroBiblioteca {

	private String titulo;
	private String codigo;
	private Boolean alquilado;
	private Integer paginas;
	private String autor;
	private String genero; 
	
	
	public LibroBiblioteca() {
		
		this.setTitulo(null);
		this.setCodigo(null);
		this.setAlquilado(null);
		this.setPaginas(0);
		this.setAutor(null);
		this.setGenero(null);
	}
	
	public LibroBiblioteca(String titulo, String codigo, Boolean alquilado, Integer paginas, String autor, String genero) {
		
		this.setTitulo(titulo);
		this.setCodigo(codigo);
		this.setAlquilado(alquilado);
		this.setPaginas(paginas);
		this.setAutor(autor);
		this.setGenero(genero);
	}
	
	
	public LibroBiblioteca(LibroBiblioteca libro) {
		
		this.titulo = new String(libro.titulo);
		this.codigo = new String(libro.codigo);
		this.alquilado = new Boolean(libro.alquilado);
		this.paginas = new Integer(libro.paginas);
		this.autor = new String(libro.autor);
		this.genero = new String(libro.genero);
	}
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public Boolean getAlquilado() {
		return alquilado;
	}
	public void setAlquilado(Boolean alquilado) {
		this.alquilado = alquilado;
	}
	public Integer getPaginas() {
		return paginas;
	}
	public void setPaginas(Integer paginas) {
		this.paginas = paginas;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
}
