package Practica7Ejercicio2;

public class LibroLibreria {

	private String titulo;
	private Integer paginas;
	private Integer precio;
	private String autor;
	private String genero;
	
	public LibroLibreria() {
		
		this.setTitulo(null);
		this.setPaginas(null);
		this.setPrecio(null);
		this.setAutor(null);
		this.setGenero(null);
	}
	
	public LibroLibreria(String titulo, Integer paginas, Integer precio, String autor, String genero) {
		
		this.setTitulo(titulo);
		this.setPaginas(paginas);
		this.setPrecio(precio);
		this.setAutor(autor);
		this.setGenero(genero);
	}
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public Integer getPaginas() {
		return paginas;
	}
	public void setPaginas(Integer paginas) {
		this.paginas = paginas;
	}
	public Integer getPrecio() {
		return precio;
	}
	public void setPrecio(Integer precio) {
		this.precio = precio;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}	
}
