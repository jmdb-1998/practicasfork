/**
 * FactorialesMetodo.java
 * Programa que mustra los factoriales de un numero.
 * jmdb - 2020.12.01
 */

package Practica3;

import java.util.Scanner;

public class FactorialesMetodo {

	Scanner teclado = new Scanner(System.in);

	public static void main(String argumentos[]) {

		Scanner teclado = new Scanner(System.in);

		System.out.println("Por favor introduzca el numero que quiere factorizar: ");
		double numero = teclado.nextDouble();

		factorial(numero);

		teclado.close();
	}

	public static void factorial(double numero) {
		double factorial = 1;

		if (numero < 0) {
			System.out.println("El programa no admite numeros negativos");
		}else {
			while ( numero != 0) {
				factorial = factorial * numero;
				numero--;
			}
			System.out.println("El numero factorizado da como resultado " + factorial);
		} 
	}	

}