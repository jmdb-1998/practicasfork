/**
 * Triangulo.java
 * Programa que Introduces un n�mero y te realiza un tri�ngulo con ese numero de pisos.
 * jmdb - 2020.11.27
 */

package Practica3;

import java.util.Scanner;

public class Triangulo {

	public static void main(String argumentos[]) {

		Scanner teclado = new Scanner(System.in);

		System.out.println("Por favor introduzca el numero de niveles de la piramide ");
		int numero = teclado.nextInt();
		int contador = numero;

		for (int i = 0; i < numero; i++) {

			System.out.println(espacios(contador) + asteriscos(i));
			contador--;
		}
		teclado.close();
	}

	public static String asteriscos(int numero) { //genere los asteriscos para cada nivel

		String resultado = "";

		for (int i = -1; i < (numero * 2); i++) {
			resultado += "*";
		}	
		return resultado;
	}

	public static String espacios(int numero) { //genera los espacios en blanco necesarios para que el triangulo se vea bonito

		String resultado = "";

		for (int i = numero; i > 1; i--) {
			resultado += " ";
		}
		return resultado;
	}
}
