/**
 * SolucionesRealesMetodo.java
 * Programa que muestra la saluciones reales o compleja que tiene una ecuaci�n de segundo grado.
 * jmdb - 2020.11.28
 */

package Practica3;

import java.util.Scanner;

public class SolucionesRealesMetodo {

	public static void main(String argumentos[]) {

		Scanner teclado = new Scanner(System.in);

		double a, b, c;

		System.out.println("Por favor introduzca el primer valor");
		a = teclado.nextDouble();

		System.out.println("Por favor introduzca el segundo valor");
		b = teclado.nextDouble();

		System.out.println("Por favor introduzca el tercer valor");
		c = teclado.nextDouble();

		evaluarDiscriminante(a,b,c);

		teclado.close();
	}

	public static void evaluarDiscriminante(double a, double b, double c) {

		double d, x, y, x1, x2;


		if (a == 0 && b == 0) {

			System.out.println("La ecuaci�n que estas intentando realizar es degenerada");

		} if (a == 0 && b != 0) {

			double solucion = -c / b;

			System.out.println("La ecuaci�n solo tiene una raiz �nica y es: " + solucion);

		} else {

			d = Math.pow(b, 2) - 4 * (a * c);

			if (d >= 0) {

				x = -b / (2 * a);
				y = Math.sqrt(d) / (2 * a);

				x1 = x + y;
				x2 = x - y;

				System.out.println("Exsiten 2 ra�ces reales, que son: " + x1 + " y " + x2);

			} else {

				d = d * -1;
				x = -b / (2 * a);
				y = Math.sqrt(d) / (2 * a);

				x1 = x + y;
				x2 = x - y;	

				System.out.println("Exsiten 2 ra�ces complejas, que son: " + x1 + " y " + x2);
			}
		}
	}
}