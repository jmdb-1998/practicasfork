/**
 * Rombo.java
 * Programa que Introduces un n�mero IMPAR y natural y te realiza un rombo con esos parametros.
 * jmdb - 2020.11.27
 */

package Practica3;

import java.util.Scanner;

public class Rombo {

	private static Scanner sc;
    
    public static void main(String[] args) {
    	
    	Scanner teclado = new Scanner(System.in);
    	
        boolean numeroCorrecto = false;
        int numFilasRombos; 
        
        do {
            System.out.print("Introduce el n�mero de filas (natural e impar): ");
            
            numFilasRombos = teclado.nextInt();
            
            if(numFilasRombos > 0 && numFilasRombos % 2 != 0){
                numeroCorrecto = true;
            }
        } while (!numeroCorrecto);
         
        rombo(numFilasRombos);
    }
	
    public static void rombo(int numFilasRombos) {

		System.out.println("");
        
        int numFilas = numFilasRombos/2 + 1;
                 
        for(int altura = 1; altura <= numFilas; altura++){
            
            for(int blancos = 1; blancos <= numFilas - altura; blancos++){
                System.out.print(" ");
            }
            
            for(int asteriscos = 1; asteriscos <= (2 * altura) - 1; asteriscos++){
                System.out.print("*");
            }
            System.out.println("");         
        }
         
        numFilas--;
         
        for(int altura = 1; altura <= numFilas; altura++){
            
            for(int blancos = 1; blancos <= altura; blancos++){
                System.out.print(" ");
            }
            
            for(int asteriscos = 1; asteriscos <= (numFilas - altura) * 2 + 1; asteriscos++){
                System.out.print("*");
            }
            System.out.println();
        }
	}	
}

