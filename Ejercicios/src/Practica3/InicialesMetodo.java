/**
 * InicialesMetodo.java
 * Programa que llama a un metodo y muestra las iniciales por pantalla
 * jmdb - 2020.12.02
 */

package Practica3;

public class InicialesMetodo {

	public static void main(String[] args) {

		iniciales();

	}
	
	public static void iniciales() {

		System.out.println("H     H   TTTTTTT     AAA"+ "\n" + "H     H      T       A   A" + "\n" + "H     H      T      A     A"+ "\n" + "HHHHHHH      T      A     A" + "\n" + "H     H      T      AAAAAAA" + "\n" + "H     H      T      A     A" + "\n" + "H     H      T      A     A");
	}
}
