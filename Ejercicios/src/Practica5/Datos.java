package Practica5;


/**
 * Datos.java
 * Clase que contiene un array de objetos de la clase Usuario.
 * jmdb - 2021.03.03
 */

public class Datos {

    public static Usuario[] datosUsuarios;

    public static void main(String[] args) {

    	String texto = "00000000T,,Pepe,,L�pez P�rez,,C/Luna,27,30132, Murcia,,pepe0@gmail.com,,1990.11.12,,2015.12.3,,miau0,,NORMAL;00000000T,,Pepe,,L�pez P�rez,,C/Luna,27,30132, Murcia,,pepe0@gmail.com,,1990.11.12,,2015.12.3,,miau0,,NORMAL;";

    	importarUsuariosTexto(texto);

    }

	private static void importarUsuariosTexto(String texto) {
			
		String [] usuarios = texto.split(";");
		
		datosUsuarios = new Usuario[usuarios.length + 20];
		
		for (int i = 0; i < texto.length(); i++) {
			
			
			String[] datosPersonas = usuarios[i].split(",,");
			datosUsuarios[i] = new Usuario();
			datosUsuarios[i].nif = datosPersonas[0];
			datosUsuarios[i].nombre = datosPersonas[1];
			datosUsuarios[i].apellidos = datosPersonas[2];
			datosUsuarios[i].domicilio = datosPersonas[3];
			datosUsuarios[i].correo = datosPersonas[4];
			datosUsuarios[i].fechaNacimiento = datosPersonas[5];
			datosUsuarios[i].fechaAlta = datosPersonas[6];
			datosUsuarios[i].claveAcceso = datosPersonas[7];
			datosUsuarios[i].rol = datosPersonas[8];
		}
			
	}

    // Otros m�todos...

} //class


class Usuario {

    public enum RolUsuario {
        NORMAL,
        INVITADO,
        ADMIN,
    }    

    private Nif nif;
    private String nombre;
    private String apellidos;
    private DireccionPostal domicilio;
    private Correo correo;
    private Fecha fechaNacimiento;
    private Fecha fechaAlta;
    private ClaveAcceso claveAcceso;
    private RolUsuario rol;
    
    public String toString() {
		return "Usuario [nif=" + nif + ", nombre=" + nombre + ", apellidos=" + apellidos + ", domicilio=" + domicilio
				+ ", correo=" + correo + ", fechaNacimiento=" + fechaNacimiento + ", fechaAlta=" + fechaAlta
				+ ", claveAcceso=" + claveAcceso + ", rol=" + rol + "]";
	}

} //class
