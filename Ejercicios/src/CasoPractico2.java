
/**
 * Programa que lee una serie de números naturales y muestra en
 * la pantalla el mayor, menor y el total de datos válidos leídos.
 * 
 * 
 * Autor: Héctor Castro Gómez.
 */

import java.util.Scanner;						
import java.util.InputMismatchException;						//Librería que permite la captura de excepciones (datos mal introducidos);

public class CasoPractico2 {

	public static void main(String[] args) {


		int valorEntrada = 0;
		int elMayor = 1;										//Se inicializa así para que cualquier valor excepto él mismo sea el mayor.
		int elMenor = 2147483647;								//Mismo razonamiento pero al revés. Cualquier dato introducido será menor, excepto si es el mismo.
		int contadorDatosValidos = 0;							//Variable que indica el número de datos válidos.
		boolean confirmacion = false;							//Variable utilizada para la repetición del bucle de la confirmación de salida del programa pulsando la tecla t.
		boolean repeticion = true;								//Variable utilizada para la repetición del bucle de entrada de los números enteros cuando el número de datos válidos es menor de 2.
		boolean repeticion2 = true;								//Variable utilizada para la repetición del bucle de entrada de los números enteros cuando el número de datos válidos es mayor o igual a 2.
		
		System.out.println("Teclea números entre 1 y 2147483647 (al menos 2)\n\n -Cero para terminar-\n");


		while (contadorDatosValidos < 2) {						//Bucle que sirve para que obligue al programa a introducir al menos 2 datos válidos.

			repeticion = true;

			while(repeticion == true) {							//Repetición del bucle de entrada de los números enteros.

				Scanner teclado = new Scanner (System.in);		//Creación de un objeto de tipo Scanner llamado teclado, para introducir datos.

				try {											//try y catch se usan para que el programa siga su funcionamiento aunque se produzca una excepción,
																//es decir, la introducción de un dato que no sea un entero. Si se introduce un dato que no sea un
					valorEntrada = teclado.nextInt();			//entero, este bucle while se repetirá y volverá por tanto a pedir la introducción de un dato válido.

					if (valorEntrada < 0) {						//Este condicional se asegura de que los números introducidos sean naturales positivos.

						System.out.println("\nLo siento, solo se permite la entrada de números naturales (enteros positivos sin el 0).\n");
						repeticion = true;

					}

					else {										//Si se introduce un dato válido, se sale del bucle.

						repeticion = false;

					}

				}

				catch (InputMismatchException ex) {				//Catch lleva el mensaje que aparecerá si se introduce un dato no entero.

					System.out.println("\nNo ha introducido un número de tipo Int, inténtelo de nuevo:\n");
					repeticion = true;
					teclado.next();

				}

			}		

			if (valorEntrada == 0){								//Este if lanza una advertencia de que no debemos introducir un 0 antes de haber ingresado 2 datos
																//válidos, en el caso de que lo hagamos.
				System.out.println("\nLo siento, debe introducir al menos dos datos válidos distintos de 0.\n");

			}

			else {												

				if (valorEntrada > elMayor){					//Si el valor introducido es el dato mayor de todos los ingresados hasta ahora, se guarda en la 
																//variable elMayor.
					elMayor = valorEntrada;

				}

				if (valorEntrada < elMenor) {					//Si el valor introducido es el dato menor de todos los ingresados hasta ahora, se guarda en la 
																//variable elMenor.
					elMenor = valorEntrada;

				}

				contadorDatosValidos++;							//Se suma uno al número de datos válidos.

			}

		}


		while (valorEntrada != 0) {								//Este bucle hace lo mismo que el anterior, con la diferencia de que ahora la introducción de un 0
																//provocará la salida del programa. Se ejecuta cuando el número de datos válidos es igual o mayor 
			Scanner teclado = new Scanner (System.in);			//que dos.
			
			repeticion2 = true;
			
			while (repeticion2 == true) {						//Repetición del segundo bucle de entrada de los números enteros.
				
				try {											//try y catch se usan para que el programa siga su funcionamiento aunque se produzca una excepción,
																//es decir, la introducción de un dato que no sea un entero. Si se introduce un dato que no sea un
					valorEntrada = teclado.nextInt();			//entero, este bucle while se repetirá y volverá por tanto a pedir la introducción de un dato válido.

					if (valorEntrada < 0) {						//Este condicional se asegura de que los números introducidos sean naturales positivos.

						System.out.println("\nLo siento, solo se permite la entrada de números naturales (enteros positivos sin el 0).\n");
						repeticion2 = true;

					}

					else {										//salida del bucle si se introduce un dato permitido.

						repeticion2 = false;

					}

				}

				catch (InputMismatchException ex) {				//Catch lleva el mensaje que aparecerá si se introduce un dato no entero.

					System.out.println("\nNo ha introducido un número de tipo Int, inténtelo de nuevo:\n");
					repeticion2 = true;
					teclado.next();

				}
			}
		
			if (valorEntrada > elMayor && valorEntrada != 0){

				elMayor = valorEntrada;

			}

			if (valorEntrada < elMenor && valorEntrada != 0) {	//Se toma la precaución de que en la primera iteración del bucle que se ejecuta cuando el número
																//de datos válidos es exactamente 2 no sea un 0, puesto que en ese caso se tomaría como dato menor
				elMenor = valorEntrada;							//el 0;

			}

			contadorDatosValidos++;								//Se suma uno al número de datos válidos.

		}

		while (confirmacion == false) {							//Bucle para la confirmación de salida del programa pulsando la tecla t.

			System.out.println("\nPulsa la tecla T para salir...\n");
			
			Scanner teclado2 = new Scanner (System.in);
			char pregunta = teclado2.next().charAt(0);			//Tras imprimir el mensaje en pantalla, se pide la introducción de un carácter.

			if (pregunta == 't') {								//Se comprueba que el carácter sea t, y en caso afirmativo se procede a imprimir en pantalla el total de datos
																//válidos, el número mayor, y el número menor.
				confirmacion = true;

				System.out.println("\nTotal de datos válidos: " + (contadorDatosValidos -1));	//Se resta uno para que no cuente el 0 como último dato válido.
				System.out.println("El mayor: " + elMayor);
				System.out.println("El menor: " + elMenor);

			}

			else {												//En caso de no haberse pulsado la t, el bucle se repite y te pide que la introduzcas otra vez.

				System.out.println("Lo siento, no ha pulsado la tecla t, inténtelo de nuevo.");

			}


		}

	}

}
