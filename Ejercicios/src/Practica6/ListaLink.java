/**
 * ListaLink.java
 * Forma para borrar elementos ne una lista enlazada
 * jmdb - 2021.04.08
 */

package Practica6;

public class ListaLink {

	/**
	 * La clase Nodo est� anidada y representa la estructura de un elemento de
	 * la lista enlazada.
	 */
	class Nodo {

		//Atributos
		Object dato;
		Nodo siguiente;
		Nodo anterior;

		/**
		 * Constructor que inicializa atributos al valor por defecto.
		 */
		public Nodo() {
			dato = null;
			anterior = null;            
			siguiente = null;
		}

	} //class Nodo


	//Atributos ListaLink
	private static Nodo primero;
	private Nodo ultimo;
	private static int numElementos;

	/**
	 * Constructor que inicializa los atributos al valor por defecto
	 */
	public ListaLink() {
		primero = null;
		ultimo = null;
		numElementos = 0;
	}

	private static Nodo obtenerNodo(int indice) {
		
		assert indice >= 0 && indice < numElementos; //Comprobar comprobamos que el indice esta dentro del rango
		
		// Recorre la lista hasta llegar al nodo  de posici�n buscada.
		
		Nodo actual = primero;
		
		for (int i = 0; i < indice; i++)
			actual = actual.siguiente;
		
		return actual;
	}


	public static Object remove(Object dato) { 	

		// Obtiene el �ndice del elemento especificado.
		int actual = indexOf(dato);
		if (actual != -1) {
			remove(actual);        // Elimina por �ndice.
		}
		return actual;
	}

	public static int indexOf(Object dato) {
		Nodo actual = primero;
		for (int i = 0; actual != null; i++) {
			if ((actual.dato != null && actual.dato.equals(dato))
					|| actual.dato == dato) {
				return i;
			}
			actual = actual.siguiente;
		}
		return -1;
	}


	public static Object remove(int indice) {
		// Lanza excepci�n si el �ndice no es v�lido.
		if (indice >= numElementos || indice < 0) {
			throw new IndexOutOfBoundsException("�ndice incorrecto: " + indice);
		}		
		if (indice > 0) {		
			return removeIntermedio(indice);
		}		
		if (indice == 0) {
			return removePrimero();
		}		
		return null;
	}

	/**
	 * Elimina el primer elemento.
	 * @return - el elemento eliminado o null si la lista est� vac�a.
	 */
	private static Object removePrimero() {

		//variables auxiliares
		Nodo actual = null;
		actual = primero;			   // Guarda actual.
		primero = primero.siguiente;	   // Elimina elemento del principio.
		numElementos--;
		return actual.dato;
	}

	/**
	 * Elimina el elemento indicado por el �ndice. 
	 * @param indice - posici�n del elemento a eliminar.
	 * @return - el elemento eliminado o null si la lista est� vac�a.
	 */
	private static Object removeIntermedio(int indice) {

		assert indice > 0 && indice < numElementos;
		//variables auxiliares
		Nodo actual = null;
		Nodo anterior = null;
		// Busca nodo del elemento anterior correspondiente al �ndice.
		anterior = obtenerNodo(indice - 1);
		actual = anterior.siguiente;		     // Guarda actual.
		anterior.siguiente = actual.siguiente;      // Elimina el elemento.
		numElementos--;
		return actual.dato;	
	}

}
