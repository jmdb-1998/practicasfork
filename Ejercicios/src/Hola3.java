/** Supuesto Practico
 * Curso: 1� DAW
 *  Alumno: Juan Manuel Delgado Barranco
 */

import java.util.Scanner;

public class Hola3 {

	@SuppressWarnings("resource")
	public static void main(String argumentos[]) {     
        //Aqui podevos ver el main, del programa, esto es obligarotorio para que el programa funcione
        // Aqui podemos ver como al escanner le damos el nombre de teclado para que sea mas sencillo utiilzaro
        Scanner teclado = new Scanner(System.in);
        int num;               //  Esto es la creaci�n de a variable num que es un integer
        
        // Despues de crear las variables necesarias comenzamos con el programa
        num = 1;
        System.out.println("Hola, Soy un modesto ");
        System.out.print("programa de ordenador...\n");
        System.out.println("Mi n�mero de orden es el " + num + " por ser el primero.");
        // Aqui la variabe num se le da el vaor de 1 y se muestra por pantalla junto con un texto
        System.out.println("Escribe dos n�meros...\n");
        int dato1 = teclado.nextInt();
        int dato2 = teclado.nextInt();
        // En las ineas de arriba o que se hace es recoger los datos introducidos por el teclado y guardarlos en las variables dato1 y dato2
        System.out.println("Dato1: " + dato1);
        System.out.println("Dato2: " + dato2);
        // En las lineas de arriba se mostrara por pantalla el texto Dato1 y siguiendo los datos introducidos por teclado 
        String mensaje = "";
        if (dato1 == dato2) {
            mensaje = "Ninguno de los dos es mayor... ";
        }
        if (dato1 > dato2) {
            mensaje = "El mayor es: " + dato1;
        }
        if (dato2 > dato1) {
            mensaje = "El mayor es: " + dato2;
        }
        System.out.println(mensaje);
    }
	
}
