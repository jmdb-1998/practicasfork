/*---------Lista en Array Estatico */
public class ListaArray {

	// Atributos 
	private Object[] arrayElementos;
	private int numElementos;
	private static final int TAMA�O_INICIAL = 4;

	// M�todos  
	/** 
	 * Inicializa el array de elementos de la lista.  
	 */
	public ListaArray() {
		arrayElementos = new Object[TAMA�O_INICIAL];
		numElementos = 0;
	}

	/**
	 * @return n�mero de elementos actual en la lista.
	 */
	public int size() {
		return numElementos;
	}

	// ...

} // class

/**
 * A�ade un elemento a la lista
 * @param elemento - el elemento a a�adir
 */
public void add(Object elemento) {
	if (numElementos == 0) {
		arrayElementos[0] = elemento;
		numElementos++;
	}
	else {
		comprobarLlenado();
		arrayElementos[numElementos] = elemento;
		numElementos++;
	}
}

/**
 * Comprueba si el array si el array interno est� casi lleno y lo copia
 * ampliando al doble su tama�o.
 */
private void comprobarLlenado() {
	// El array interno est� casi lleno, se duplica el espacio. 
	if (numElementos + 1 == arrayElementos.length) {
		Object[] arrayAmpliado = new Object[arrayElementos.length*2];
		System.arraycopy(arrayElementos, 0, arrayAmpliado, 0, numElementos);
		arrayElementos = arrayAmpliado;
	}
}

/**
 * Inserta un elemento en la posici�n especificada por el �ndice.
 * @param indice - indica la posici�n de inserci�n en la lista.
 * @param elemento - elemento a insertar.
 * @throws IndexOutOfBoundsException
 */
public void add(int indice, Object elemento) {
	// El �ndice debe ser v�lido.
	if (indice >= numElementos || indice < 0) {
		throw new IndexOutOfBoundsException("�ndice incorrecto: " 
				+ indice);
	}
	comprobarLlenado();

	// Inserci�n, desplaza los elementos desde �ndice indicado.
	if (indice < numElementos) {
		System.arraycopy(arrayElementos, indice, arrayElementos, indice+1, numElementos - indice);
	}
	arrayElementos[indice] = elemento;
	numElementos++;
}

/**
 * Devuelve el �ndice de la primera ocurrencia para el objeto especificado.
 * @param elem - el elemento buscado.
 * @return el �ndice del elemento o -1 si no lo encuentra.
 */
public int indexOf(Object elem) {
	if (elem == null) {
		for (int i = 0; i < arrayElementos.length; i++) {
			if (arrayElementos[i] == null) {
				return i;
			}
		}
	} 
	else {
		for (int i = 0; i < arrayElementos.length; i++) {
			if (elem.equals(arrayElementos[i])) {
				return i;
			}
		}
	}
	return -1;
}

/**
 * Borra todos los elementos de la lista.
 */
public void clear() {
	arrayElementos = new Object[TAMA�O_INICIAL];
	numElementos = 0;
}

/**
 * Comprueba si existe un elemento.
 * @param elem � el elemento a comprobar.
 * @return true - si existe.
 */
public boolean contains(Object elem) {
	return indexOf(elem) != -1; 
}

/**
 * Obtiene el elemento-dato por �ndice.

 * @param indice - posi�n relativa del nodo que contiene el elemento-dato.
 * @return el dato indicado por el �ndice de nodo; null si est� indefinido.
 *@exception IndexOutOfBoundsException - �ndice no est� entre 0 y numElementos-1.
 */
public Object get(int indice) {
	// El �ndice debe ser v�lido para la lista.
	if (indice >= numElementos || indice < 0) {
		throw new IndexOutOfBoundsException("�ndice incorrecto: " + indice);
	}  
	return arrayElementos[indice];
}

/**
 * Elimina el elemento especificado en el �ndice.
 * @param indice - del elemento a eliminar.
 * @return - el elemento eliminado.
 * @exception IndexOutOfBoundsException - �ndice no est� entre 0 y numElementos-1.
 */
public Object remove(int indice) {
	// El �ndice debe ser v�lido para la lista.
	if (indice >= numElementos || indice < 0) {
		throw new IndexOutOfBoundsException("�ndice incorrecto: " + indice);
	}
	// Elimina desplazando uno hacia la izquierda, sobre la posici�n a borrar.
	Object elem = arrayElementos[indice];
	System.arraycopy(arrayElementos, indice+1, arrayElementos, indice, numElementos - (indice+1));

	// Ajusta el �ltimo elemento.
	arrayElementos[numElementos-1] = null;
	numElementos--;
	return elem;
}

/**
 * Elimina el elemento especificado.
 * @param elemento - elemento a eliminar.
 * @return - el �ndice del elemento eliminado o -1 si no existe.
 */
public int remove(Object elem) {
	int indice = indexOf(elem);

	if (indice != -1) {
		remove(indice);
	}        
	return indice;
}


/*----------------------Lista Enlazada------------------------*/




/**
 * Representa la implementaci�n m�s sencilla y b�sica de una lista enlazada simple
 * con acceso s�lo al principio de la serie de nodos.
 */
public class ListaEnlazada {
	// Atributos
	private Nodo primero;      // Referencia a nodo
	private int numElementos;

	// M�todos
	/**
	 * Constructor que inicializa los atributos al valor por defecto.
	 * Lista vac�a.
	 */
	public ListaEnlazada() {
		primero = null;
		numElementos = 0;
	}

	//...

} // class

/** 
 * Representa la estructura de un nodo para una lista din�mica con enlace simple.
 */
class Nodo {
	// Atributos
	Object dato;
	Nodo siguiente; 
	/**
	 * Constructor que inicializa atributos por defecto.
	 * @param elem - el elemento de informaci�n �til a almacenar.
	 */
	public Nodo(Object dato) {
		this.dato = dato;
		siguiente = null;
	}

} // class

/**
 * A�ade un elemento al final de la lista.
 * @param elem - el elemento a a�adir.
 * Admite que el elemento a a�adir sea null.
 */
public void add(Object dato) {   
	//variables auxiliares
	Nodo nuevo = new Nodo(dato);
	Nodo ultimo = null;
	if (numElementos == 0) {
		// Si la lista est� vac�a enlaza el nuevo nodo el primero.
		primero = nuevo;
	}
	else {
		// Obtiene el �ltimo nodo y enlaza el nuevo.
		ultimo = obtenerNodo(numElementos-1);
		ultimo.siguiente = nuevo;
	}
	numElementos++;               // Actualiza el n�mero de elementos.
}

/**
 * Obtiene el nodo correspondiente al �ndice. Recorre secuencialmente la cadena de enlaces. 
 * @param indice - posici�n del nodo a obtener.
 * @return - el nodo que ocupa la posici�n indicada por el �ndice.
 */
private Nodo obtenerNodo(int indice) {
	assert indice >= 0 && indice < numElementos;
	// Recorre la lista hasta llegar al nodo  de posici�n buscada.
	Nodo actual = primero;
	for (int i = 0; i < indice; i++)
		actual = actual.siguiente;
	return actual;
}

/**
 * Elimina el elemento indicado por el �ndice. Ignora �ndices negativos
 * @param indice - posici�n del elemento a eliminar
 * @return - el elemento eliminado o null si la lista est� vac�a.
 * @exception IndexOutOfBoundsException - �ndice no est� entre 0 y numElementos-1
 */
public Object remove(int indice) {
	// Lanza excepci�n si el �ndice no es v�lido.
	if (indice >= numElementos || indice < 0) {
		throw new IndexOutOfBoundsException("�ndice incorrecto: " + indice);
	}       
	if (indice > 0) {       
		return removeIntermedio(indice);
	}       
	if (indice == 0) {
		return removePrimero();
	}       
	return null;
}

/**
 * Elimina el primer elemento.
 * @return - el elemento eliminado o null si la lista est� vac�a.
 */
private Object removePrimero() {
	//variables auxiliares
	Nodo actual = null;
	actual = primero;              // Guarda actual.
	primero = primero.siguiente;       // Elimina elemento del principio.
	numElementos--;
	return actual.dato;
}

/**
 * Elimina el elemento indicado por el �ndice. 
 * @param indice - posici�n del elemento a eliminar.
 * @return - el elemento eliminado o null si la lista est� vac�a.
 */
private Object removeIntermedio(int indice) {
	assert indice > 0 && indice < numElementos;
	//variables auxiliares
	Nodo actual = null;
	Nodo anterior = null;
	// Busca nodo del elemento anterior correspondiente al �ndice.
	anterior = obtenerNodo(indice - 1);
	actual = anterior.siguiente;             // Guarda actual.
	anterior.siguiente = actual.siguiente;      // Elimina el elemento.
	numElementos--;
	return actual.dato; 
}

/**
 * Elimina el dato especificado.
 * @param dato � a eliminar.
 * @return - el �ndice del elemento eliminado o -1 si no existe.
 */
public int remove(Object dato) { 
	// Obtiene el �ndice del elemento especificado.
	int actual = indexOf(dato);
	if (actual != -1) {
		remove(actual);        // Elimina por �ndice.
	}
	return actual;
}

/**
 * Busca el �ndice que corresponde a un elemento de la lista.
 * @param dato- el objeto elemento a buscar.
 */
public int indexOf(Object dato) {
	Nodo actual = primero;
	for (int i = 0; actual != null; i++) {
		if ((actual.dato != null && actual.dato.equals(dato))
				|| actual.dato == dato) {
			return i;
		}
		actual = actual.siguiente;
	}
	return -1;
}

/**
 * @param indice � obtiene un elemento por su �ndice.
 * @return elemento contenido en el nodo indicado por el �ndice.
 * @exception IndexOutOfBoundsException - �ndice no est� entre 0 y numElementos-1.
 */
public Object get(int indice) {
	// lanza excepci�n si el �ndice no es v�lido
	if (indice >= numElementos || indice < 0) {
		throw new IndexOutOfBoundsException("�ndice incorrecto: " + indice);
	}
	Nodo aux = obtenerNodo(indice);
	return aux.dato;
} 

/**
 * @return el n�mero de elementos de la lista
 */
public int size() {
	return numElementos;
}


/*-------Lista enlazada doble--------*/


/**
 * Representa la implementaci�n b�sica de una lista doble enlazada.
 */
public class ListaDoble {
	private Nodo primero;
	private Nodo ultimo;
	private int numElementos;

	/**
	 * Constructor que inicializa los atributos al valor por defecto.
	 */
	public ListaDoble() {
		primero = null;
		ultimo = null;
		numElementos = 0;
	}

} // class

/** 
 * Representa la estructura de un nodo para una lista enlazada doble.
 */
class Nodo {
	Object dato;
	Nodo anterior;
	Nodo siguiente;
	/**
	 * Constructor que inicializa atributos por defecto.
	 * @param dato - el elemento de informaci�n �til a almacenar.
	 */
	public Nodo(Object dato) {
		this.dato = dato;
		anterior = null;
		siguiente = null;
	}

} // class

/**
 * A�ade un elemento al final de la lista
 * @param dato - el dato del elemento a a�adir que no sea null
 */
public void add(Object dato) {
	addUltimo(dato);
}

/**
 * Inserta un elemento en la posici�n indicada de la lista.
 * @param indice - posici�n donde insertar el nuevo nodo.
 * @param dato - el dato del elemento a a�adir. Admite que sea null.
 * @exception IndexOutOfBoundsException. 
 * �ndice no est� entre 0 y numElementos numElementos-1
 */
public void add(int indice, Object dato) {
	// Lanza excepci�n si el �ndice no es v�lido.
	if (indice < 0 || indice >= numElementos) {
		throw new IndexOutOfBoundsException("�ndice incorrecto: " + indice);
	}
	// Nuevo nodo al principio.
	if (indice == 0) {                         
		insertarPrimero(dato);
	}

	// Nuevo nodo en posiciones intermedias.
	if (indice > 0) {                         
		insertarIntermedio(indice, dato);                   
	}
}

/**
 * Inserta un elemento en una posici�n intermedia de una lista doble enlazada.
 * @param el �ndice que ocupar� el elemento nuevo.
 * @param el dato del elemento nuevo.
 */
private void insertarIntermedio(int indice, Object dato) {
	assert indice >= 0 && indice < numElementos;
	Nodo nuevo = new Nodo(dato);                       
	Nodo actual = obtenerNodo(indice);  // Donde insertar.
	Nodo anterior = actual.anterior;       // Obtiene el anterior.    
	actual.anterior = nuevo;            // Enlaza el nuevo nodo.
	anterior.siguiente = nuevo;             
	nuevo.anterior = anterior;             // Ajusta enlaces.
	nuevo.siguiente = actual;   
	numElementos++;                     // Actualiza tama�o.

}

/**
 * Inserta un elemento al principio de una lista doble enlazada.
 * @param el dato del elemento nuevo.
 */
private void insertarPrimero(Object dato) {                      
	Nodo nuevo = new Nodo(dato);

	// La lista est� vac�a; el nuevo nodo es primero y �ltimo.
	if (numElementos == 0) {
		primero = nuevo;
		ultimo = nuevo;
	} 
	// La lista no est� vac�a; el nuevo nodo pasa a ser el primero.
	else {
		Nodo actual = primero;      // D�nde insertar.
		actual.anterior = nuevo;            // Enlaza el nuevo nodo.
		nuevo.siguiente = actual;       // Ajusta enlace.       
		primero = nuevo;            // Actualiza el nuevo primero.
	}
	numElementos++;                     // Actualiza tama�o.
}

/**
 * A�ade un elemento al final de una lista doble enlazada.
 * @param el dato del elemento nuevo.
 */
private void addUltimo(Object dato) {
	Nodo nuevo = new Nodo(dato);                       
	// La lista est� vac�a; el nuevo nodo es �ltimo y primero.
	if (numElementos == 0) {
		ultimo = nuevo;
		primero = nuevo;        
	}
	// La lista no est� vac�a; el nuevo nodo pasa a ser el �ltimo.
	else { 
		Nodo actual = ultimo;       // D�nde insertar.
		actual.siguiente = nuevo;           // Enlaza el nuevo nodo.
		nuevo.anterior = actual;        // Ajusta enlace.       
		ultimo = nuevo;         // Actualiza el nuevo �ltimo.
	}
	numElementos++;                     // Actualiza tama�o.
}

/**
 * Obtiene el nodo correspondiente al �ndice.
 * @param indice - posici�n del nodo a obtener.
 * @return el nodo que ocupa la posici�n indicada por el �ndice.
 */
private  Nodo obtenerNodo(int indice) {
	assert indice >= 0 && indice < numElementos;
	// Recorre la lista hasta llegar a la posici�n buscada.
	Nodo actual = primero;
	for (int i = 0; i < indice; i++) {
		actual = actual.siguiente;
	}
	return actual;
}

}
,,