/**
 * MayorMenor.java
 * Programa que de una serie ilimitada de numeros introducidos por el usuario saca el mayor y el menor.
 * jmdb - 2020.11.01
 */

import java.util.InputMismatchException;
import java.util.Scanner;

public class MayorMenor {

	@SuppressWarnings({ "resource", "null", "unused" })
	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);

		int elMayor = 1, elMenor = 2147483647, contadorDatosValidos = 0;
		int valorEntrada = 0;
		String response = null;
		boolean repetir;

		System.out.println("Por favor introduzca como m�nimo dos n�mero entre 1 y 2147483647");
		
		do {
			
			try {
				valorEntrada = teclado.nextInt();
			}
			catch (InputMismatchException ex) {
				System.out.println("\nNo ha introducido un n�mero de tipo Int, int�ntelo de nuevo:\n");
				valorEntrada = teclado.nextInt();
			}

			if (valorEntrada > 0) {
				if (valorEntrada > elMayor) {

					elMayor = valorEntrada;
					contadorDatosValidos++;
				} 
				if (valorEntrada < elMenor) {

					elMenor = valorEntrada;
					contadorDatosValidos++;
				}
			} else {
				if (contadorDatosValidos-1 < 2 && contadorDatosValidos > 0 ) {
					
					System.out.println("Debes introducir 2 datos o m�s.");
					break;
				}
				
				System.out.println("�Seguro que quiere terminar?");
				response = teclado.next();

				break;
			}
		}

		while (valorEntrada > 0);
		if (valorEntrada == 0 && response.equalsIgnoreCase("T")) {
			System.out.println("Numero de datos validos " + (contadorDatosValidos - 1));
			System.out.println("El numero mayor es " + elMayor);
			System.out.println("El numero menor es " + elMenor);
		}
	}
}
