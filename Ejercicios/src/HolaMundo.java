
/**
 * HolaMundo.java
 * Programa simple que muestra un mensaje la consola del sistema.
 * ajp - 2020.10.09
 */

public class HolaMundo { //Si pones en minuscila el nombre de la clase e programa no va a funcionar
	public static void main(String[] args) {
		//System.out.println("Hola mundo...");  //Muestra mensaje 
		
		System.out.println("Nombre: Hello World\nVersi�n: 1.0\nAutor: Juan Manuel Delgado Barranco");//En esta linea con un unico println y varios saltos de l�nea (\n) he podido mostar el nombre, version y autor del programa
	}
}