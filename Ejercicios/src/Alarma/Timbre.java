package Alarma;

public class Timbre {

	private Boolean estado;

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public void activar() {
		this.estado = true;
		sonido(this.estado);
	}

	public void desActivar() {
		this.estado = false;
		sonido(this.estado);
	}

	private void sonido(Boolean estado) {
		if (this.estado == true) {
			System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA!!!!!!!!!");
		}else {
			System.out.println("Silencio Sepulcral.....................");
		}
	}
}
