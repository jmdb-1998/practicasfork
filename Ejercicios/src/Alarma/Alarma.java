package Alarma;

import java.util.Random;

public class Alarma {

	static Sensor sensor = new Sensor();

	public static Boolean comprobar(Double umbral) {

		boolean test = sensor.getValorActual(umbral);
		
		return test;
	}

	public static double generarDoubleAleatorio() {
		Random rd = new Random(); // creating Random object
		return rd.nextDouble();
	}

}
