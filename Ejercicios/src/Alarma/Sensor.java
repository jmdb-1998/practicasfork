package Alarma;

public class Sensor {

	private Double valorActual;
	
	public Sensor(Double valor) {
		this.setValorActual(valor);
	}
	
	public Sensor() {
		this.setValorActual(0.0);
	}

	public boolean getValorActual(Double valorActual) {
		Timbre t = new Timbre();
		if (valorActual >= 0.5) {
			t.activar();
			return true;
		}else {
			t.desActivar();
		}
		return false;
	}

	public void setValorActual(Double valorActual) {
		this.valorActual = valorActual;
	}
}
