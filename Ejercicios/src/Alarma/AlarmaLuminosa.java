package Alarma;

public class AlarmaLuminosa extends Alarma {

	static Bombilla bombilla = new Bombilla();
	
	public static void EncenderBombilla(Boolean valor) {
		if(valor == false) {
			bombilla.desActivar();
		}else {
			bombilla.activar();
		}
	}
	
	public static void main(String[] args) {
		
		Double umbral = generarDoubleAleatorio();
		
		EncenderBombilla(comprobar(umbral));

	}
	
}
