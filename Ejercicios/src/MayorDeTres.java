
import java.util.Scanner;


public class MayorDeTres {

	@SuppressWarnings({ "resource" })
	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		
		int num1, num2, num3;
		System.out.println("Introduzca el primer numero");
		num1 = teclado.nextInt();
		System.out.println("Introduzca el segundo numero");
		num2 = teclado.nextInt();
		System.out.println("Introduzca el tercer numero");
		num3 = teclado.nextInt();
		
		if (num1 > num2)
		{
			if (num1 > num3)
			{
				System.out.println("El mayor de los tres es " + num1);
			}else {
				System.out.println("El mayor de los tres es " + num3);
			}
		}else if (num2 > num3){
			System.out.println("El mayor de los tres es " + num2);
		}else {
			System.out.println("El mayor de los tres es " + num3);
		}
	}
}

	