/**
 * PruebaIdentificadores.java
 * Programa simple que muestra un mensaje la consola del sistema.
 * ajp - 2020.10.09
 */

 public class PruebaIdentificadores {
	@SuppressWarnings("unused")
	public static void main(String[] args) {	
		System.out.println("Hola mundo...");  //Muestra mensaje
		
		int _alpha = 6;
		
	    //int FLOAT = 3; no recomendable por el nombre
		
	    //int 1_de_muchos = 6; Los nombres de variables no pueden empezar por un numero
		
	    int maxValor = 6;
	    
	    int cuantos = 6;
	    
	    //int "dado" = 5; Los nombres de las variables no deben levar comillas
	    
	    //int Nbytes = 6; No es recomendables
	    
	    //int int  = 6; No es valido porque inte es una palabra reservada
	    
	    //int qu�_dices? = 6; No se le pueden poner caracteres especiales a el nombre de una variable
	    
	    int N�mero = 6;
	    
	    //int cadena 2 = 6; No debe haber espacios en el nombre de la variable
	    
	    int Ca��n = 6;
	    
	    int caf� = 6;
	    
	    //int Mesa-3 = 6; //no se pueden usar caracteres especiales (-)
	    
	    //int Return = 6; //no recomendable
	    
	    //int While = 6;//no recomendable
	    
	    //int __if = 6; //no recomendable
	    
	    //int Bloque#4 = 6; No se le pueden poner caracteres especiales a el nombre de una variable
	    
	    //int c o s a = 6; //espacios
	    
	    int CaPrIcHoSo = 6;
	    
	    //int 8�Roca = 6; No se le pueden poner caracteres especiales a el nombre de una variable
	    
	    //int 3d2 = 6; //comenzar por un numero
	    
	    //int Hoja3/5 = 6; No se le pueden poner caracteres especiales a el nombre de una variable
	    
	    //int pink.panther = 6; No se le pueden poner puntos a el nombre de una variable
	}
}