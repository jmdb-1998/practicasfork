/**
 * BuscarInt.java
 * Programa en entorno de pruebas al que al pasar un vectore te informa si este esta ordenado numericamente o no.
 * jmdb - 2021.01.07
 */

package Practica4;

public class BuscarInt {


	private static int [] vector1 = new int[] {2, 3, 4, 5, 6};
	private static boolean ordenado;

	public static void main(String argumentos[]) {

		buscarInt(vector1);
	}

	public static void buscarInt(int vector1[]) { 
		
		for (int i = 0; i < vector1.length; i++) {
			
            if (i + 1 < vector1.length) {
            	
                if (vector1[i] > vector1[i + 1]) {
                	
                    ordenado = false;
                    break;
                }else {
                	
                	ordenado = true;
                }
            }
        }
        if (ordenado) {
        	
            System.out.println("La lista est� ordenada");
            
        } else {
        	
            System.out.println("La lista est� desordenada");
        }
	}
	
}
