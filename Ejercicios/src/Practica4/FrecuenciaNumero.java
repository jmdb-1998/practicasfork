/**
 * FrecuenciaNumero.java
 * Programa en entorno de pruebas al que al pasar un vectore te informa del numero de veces que aparece el numero pedido.
 * jmdb - 2021.01.07
 */

package Practica4;

public class FrecuenciaNumero {

	private static int numero = 2;
	private static int [] vector = new int[] {2, 4, 5, -4, 7, 2};

	public static void main(String argumentos[]) {
		
		frecuenciaNumero(numero, vector);
	}

	public static void frecuenciaNumero(int numero, int vector[]) { 

		int contador = 0;

		for (int i = 0; i < vector.length; i++) {
			if (numero == vector[i]) {
				contador ++;	
			}
		}
		
		System.out.println("La cantidad de numeros iguales en el Vector es " + contador);
	}
	
}
