/**
 * PrimosIntervalo.java
 * Programa que requiere de 3 datos para saber que tipo de triangulo formarian los lados con esa longitud.
 * jmdb - 2020.10.09
 */

//NO ESTA ACABADO, ROLLO, FUNCIONA PERO COMO QUE ESTA MUY CURRAO

package Practica2;
import java.util.Scanner;

public class PrimosIntervalo {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		int inicio, fin, contador = 0;
		
		Scanner teclado = new Scanner(System.in);
		
		System.out.println("Escribe el inicio:");
		inicio = teclado.nextInt();
		
		System.out.println("Escribe el fin:");
		fin = teclado.nextInt();
		
		for (int i = inicio; i <= fin; i++) {
			
			if (esPrimo(i)) 
			{
				contador++;
				System.out.print(String.valueOf(i) + ",");
			}
		}
		
		System.out.printf("\nTotal: %d", contador);
	}

	public static boolean esPrimo(int numero) {
		// El 0, 1 y 4 no son primos
		if (numero == 0 || numero == 1 || numero == 4) {
			return false;
		}
		for (int x = 2; x < numero / 2; x++) {
			// Si es divisible por cualquiera de estos n�meros, no es primo
			
			if (numero % x == 0)
				return false;
		}
		// Si no se pudo dividir por ninguno de los de arriba, s� es primo
		return true;
	}
	
}
