/**
 * NumeroAleatorio.java
 * Programa que genera un numero aleatorio entre el 1 y el 10 y el usuario debe adivinarlo.
 * jmdb - 2020.10.09
 */

package Practica2;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class NumeroAleatorio {

	@SuppressWarnings({ "resource", "unused" })
	public static void main(String[] args) throws IOException {
		Scanner teclado = new Scanner(System.in);
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int numero;
		double adivinar = (1 + (int)(Math.random() * 10)); //Con esta l�nea conseguimos un numero aleatorio entre el 1 y el 10 sin ning�n decimal

		do {
		System.out.println("Por favor introduzca un n�mero");
			numero = teclado.nextInt();

			System.out.println("Presione cualquier tecla");
			
			String continuar = br.readLine();
		}

		while (numero < 0 || numero > 10);

		if (numero == adivinar)
		{

			System.out.println("Enhorabuena, has adivinado el n�mmero");

		}else {

			System.out.println("Mala suerte, intentalo de nuevo");
		}

	}
}
