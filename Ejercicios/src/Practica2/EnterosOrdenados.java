/**
 * DatosPersonaes.java
 * Programa que recibe 3 datos y los muestra de mayor a menor.
 * jmdb - 2020.10.31
 */

package Practica2;
import java.util.Scanner;
public class EnterosOrdenados {

	@SuppressWarnings("resource")
	public static void main(String[] args) {


		Scanner teclado = new Scanner(System.in);
		System.out.println("Ingrese tres n�meros: ");
		System.out.println("");

		int dato1 = teclado.nextInt();
		int dato2 = teclado.nextInt();
		int dato3 = teclado.nextInt();

		int menor = (Math.min(dato3, Math.min(dato1, dato2)));
		int mayor = (Math.max(dato3, Math.max(dato1, dato2)));
		int suma = dato1 + dato2 + dato3;

		if ((dato1 != dato2) && (dato2 != dato3) && (dato3 != dato1)){

			int medio = suma - mayor - menor;

			System.out.println("Los n�meros ordenados de mayor a menor son:");
			System.out.println("");
			System.out.println(mayor);
			System.out.println(medio);
			System.out.println(menor);

		}else {
			if ((dato1 == dato2) && (dato1 == dato3)) {
				System.out.println("Los tres datos son iguales");
			}else {
				if (suma - (mayor * 2) == menor) {
					System.out.println("Hay dos datos con el valor " + mayor + ", que son mayores que " + menor + ".");
				}else {
					System.out.println("Hay dos datos con el valor " + menor + ", que son menores que " + mayor + ".");
				}
			}
		}
	}
}
