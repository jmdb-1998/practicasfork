/**
 * VariablesEntero.java
 * Programa simple que declare tres variables de tipo entero, asigne valor diferente a cada una y las muestre por pantalla.
 * jmdb - 2020.27.10
 */
package Practica2;

public class VariablesEntero {

	public static void main(String[] args) {
		
		int var1 = 10;
		int var2 = 100;
		int var3 = 1000;
		
		System.out.println("Estas son las 3 variables de tipo entero " + var1 +" "+ var2 +" "+ var3);
	}
}




