/**
 * Calcuadora.java
 * Programa que utiliza el switch case para formar una calculadora.
 * jmdb - 2020.11.01
 */

package Practica2;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Calculadora {

	@SuppressWarnings("resource")
	public static void main(String[] args) {

		Scanner teclado = new Scanner(System.in);
		boolean salir = false;
		int opcion; //Aqui guardaremos la opcion elegida por el usuario

		while (!salir) {

			System.out.println("1: Sumar");
			System.out.println("2: Restar");
			System.out.println("3: Multiplicar");
			System.out.println("4: Dividir");
			System.out.println("5: Terminar");

			try {
				System.out.println("Seleccione una opci�n");

				opcion = teclado.nextInt();

				switch (opcion) {

				case 1:
					double num1, num2, resultado_suma;
					System.out.println("Por favor, introduzca el primer n�mero ");
					num1 = teclado.nextDouble();
					System.out.println("Por favor introduzca el segundo n�mero ");
					num2 = teclado.nextDouble();
					resultado_suma = num1 + num2;
					System.out.println("La suma de los dos es " + resultado_suma);
					break;

				case 2:
					double num3, num4, resultado_resta;
					System.out.println("Por favor, introduzca el primer n�mero ");
					num3 = teclado.nextDouble();
					System.out.println("Por favor introduzca el segundo n�mero ");
					num4 = teclado.nextDouble();
					resultado_resta = num3 - num4;
					System.out.println("La resta de los dos es " + resultado_resta);
					break;

				case 3:
					double num5, num6, resultado_multi;
					System.out.println("Por favor, introduzca el primer n�mero ");
					num5 = teclado.nextDouble();
					System.out.println("Por favor introduzca el segundo n�mero ");
					num6 = teclado.nextDouble();
					resultado_multi = num5 * num6;
					System.out.println("La multipicaci�n de los dos es " + resultado_multi);
					break;

				case 4:
					double num7, num8, resultado_divi;
					System.out.println("Por favor, introduzca el primer n�mero ");
					num7 = teclado.nextDouble();
					System.out.println("Por favor introduzca el segundo n�mero ");
					num8 = teclado.nextDouble();
					resultado_divi = num7 / num8;
					System.out.println("La divisi�n de los dos es " + resultado_divi);
					break;

				case 5:
					salir = true;
					break;

				default:
					System.out.println("Por favor, introduzca un numero entre el 1 y el 4");

				}

			} catch (InputMismatchException e){
				System.out.println("Debes insertar un numero");
				teclado.next();
			}
		}


	}
}
