/**
 * Cuadrado.java
 * Programa al que le introduces un n�mero y te muestra en la consola un cuadrado con el mismo numero de asteriscos por lados.
 * jmdb - 2020.11.01
 */

package Practica2;
import java.util.Scanner;

public class Cuadrado {

	@SuppressWarnings("resource")
	public static void main(String[] args) {

		Scanner teclado = new Scanner(System.in);

		System.out.print("Por favor ingrese el numero de asteriscos: ");
		int n = teclado.nextInt();

			//Linea superior
			for(int i = 0; i < n; i++) {
				System.out.print("* ");
			}
			System.out.println();

			//Centro del cuadrado
			for(int i = 0; i < n-2; i++) {
				System.out.print("* ");
				for(int j = 0; j < n-2; j++) {
					System.out.print("  ");
				}
				System.out.println("* ");
			}

			//Linea inferior
			for(int i = 0; i < n; i++) {
				System.out.print("* ");
			}
	}
}
