/**
 * MayorDeTodos.java
 * Programa al que se le introducen datos hasta que el usuario quiera parar introducinedo el caracter "n". 
 * Una vez que el programa para mostrara en pantalla el numero m�s grande introducido.
 * jmdb - 2020.11.04
 * Este programa solo funciona con los caracteres s y n, cualquier otra cosa no la acepta
 */

package Practica2;
import java.util.Scanner;

public class MayorDeTodos {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);

		int resultado = 0;
		int comprobar;
		String response;
		
		do {
			System.out.println("Por favor introduzca un n�mero");
			comprobar = teclado.nextInt();

			if (comprobar > resultado) {
				resultado = comprobar;
			}
		
		System.out.println("�Desea continuar?");
		response = teclado.next();}
		
		while (response.equalsIgnoreCase("s"));
		if (response.equalsIgnoreCase("n")) {
			System.out.println("El numero mayor es " + resultado);
		}
	}
}
