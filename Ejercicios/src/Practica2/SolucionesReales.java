/**
 * SolucionesReales.java
 * Programa que muestraLa soluci�n a las ecuaciones de segundo grado
 * jmdb - 2020.11.04
 */
package Practica2;
import java.util.Scanner;

public class SolucionesReales {

	@SuppressWarnings("resource")
	public static void main(String[] args) {

		Scanner teclado = new Scanner(System.in);

		double a, b, c, d, x, y, x1, x2;

		System.out.println("Por favor introduzca el primer valor");
		a = teclado.nextDouble();

		System.out.println("Por favor introduzca el segundo valor");
		b = teclado.nextDouble();

		System.out.println("Por favor introduzca el tercer valor");
		c = teclado.nextDouble();


		if (a == 0 && b == 0) {

			System.out.println("La ecuaci�n que estas intentando realizar es degenerada");

		} if (a == 0 && b != 0) {

			double solucion = -c / b;

			System.out.println("La ecuaci�n solo tiene una raiz �nica y es: " + solucion);

		} else {

			d = Math.pow(b, 2) - 4 * (a * c);

			if (d >= 0) {

				x = -b / (2 * a);
				y = Math.sqrt(d) / (2 * a);

				x1 = x + y;
				x2 = x - y;

				System.out.println("Exsiten 2 ra�ces reales, que son: " + x1 + " y " + x2);

			} else {
				
				d = d * -1;
				x = -b / (2 * a);
				y = Math.sqrt(d) / (2 * a);

				x1 = x + y;
				x2 = x - y;	

				System.out.println("Exsiten 2 ra�ces complejas, que son: " + x1 + " y " + x2);
			}
		}

	}
}
