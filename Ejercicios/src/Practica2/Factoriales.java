/**
 * Factoriales.java
 * Programa que saca los factoriales de un numero.
 * jmdb - 2020.11.01
 */
package Practica2;
import java.util.Scanner;

public class Factoriales {
	
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		double factorial = 1;
		double numero;

		Scanner teclado = new Scanner(System.in);
		
		System.out.println("Por favor introduzca el numero que quiere factorizar: ");
		numero = teclado.nextDouble();
		
		if (numero < 0) {
			System.out.println("El programa no admite numeros negativos");
		}else {
			while ( numero != 0) {
				  factorial = factorial * numero;
				  numero--;
				}
			System.out.println(factorial);
		}
		
	}

}
