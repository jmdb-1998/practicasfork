import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.InputMismatchException;
import java.util.Scanner;

public class pruebas {
	public static void main(String[] args) {
		Scanner teclado2 = new Scanner (System.in);

		int valorEntrada = 0;
		int elMayor = -2147483648;
		int elMenor = 2147483647;
		int contadorDatosValidos = 0;
		boolean confirmacion = false;
		boolean repeticion = true;

		System.out.println("Teclea n�meros entre 1 y 2147483647 (al menos 2)\n\n -Cero para terminar-\n");

		while (contadorDatosValidos < 2) {
			repeticion = true;
			while(repeticion == true) {
				Scanner teclado = new Scanner (System.in);
				try {
					valorEntrada = teclado.nextInt();
					repeticion = false;
				}
				catch (InputMismatchException ex) {
					System.out.println("\nNo ha introducido un n�mero de tipo Int, int�ntelo de nuevo:\n");
					repeticion = true;
					teclado.next();
				}
			}
			if (valorEntrada == 0){
				System.out.println("\nLo siento, debe introducir al menos dos datos v�lidos distintos de 0.\n");
			}
			else {
				if (valorEntrada > elMayor){
					elMayor = valorEntrada;
				}
				if (valorEntrada < elMenor) {
					elMenor = valorEntrada;
				}
				contadorDatosValidos++;
			}
		}

		while (valorEntrada != 0) {

			Scanner teclado = new Scanner (System.in);
			valorEntrada = teclado.nextInt();

			if (valorEntrada > elMayor){

				elMayor = valorEntrada;

			}

			if (valorEntrada < elMenor && valorEntrada != 0) {

				elMenor = valorEntrada;

			}

			contadorDatosValidos++;

		}

		while (confirmacion == false) {

			System.out.println("\nPulsa la tecla T para salir...\n");

			char pregunta = teclado2.next().charAt(0);

			if (pregunta == 't') {

				confirmacion = true;

				System.out.println("\nTotal de datos v�lidos: " + (contadorDatosValidos -1));
				System.out.println("El mayor: " + elMayor);
				System.out.println("El menor: " + elMenor);

			}

			else {

				System.out.println("Lo siento, no ha pulsado la tecla t, int�ntelo de nuevo.");

			}


		}

	}

}
